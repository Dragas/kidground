package lt.saltyjuice.dragas.kidground.core.playsite;

import lt.saltyjuice.dragas.kidground.core.impl.SizedPlaysite;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DoubleSwing extends SizedPlaysite {
    public DoubleSwing() {
        super(2);
    }

    @Override
    public BigDecimal getCurrentUsage() {
        if(isFull()) {
            return BigDecimal.ONE.divide(BigDecimal.ONE, 10, RoundingMode.HALF_UP);
        }
        else {
            return BigDecimal.ZERO.divide(BigDecimal.ONE, 10, RoundingMode.HALF_UP);
        }
    }
}
