package lt.saltyjuice.dragas.kidground.core.impl;

import lt.saltyjuice.dragas.kidground.api.*;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Implements general expectations of {@link PlaysiteQueue} interface
 */
public class DefaultQueue implements PlaysiteQueue {
    private final Playsite playsite;
    private final List<Child> queue = new LinkedList<>();

    public DefaultQueue(Playsite playsite) {
        this.playsite = playsite;
    }

    @Override
    public void enqueue(Child child) throws TantrumException {
        if (!child.canWaitInQueue() && (!queue.isEmpty() || playsite.isFull())) {
            throw new TantrumException("Child cannot wait in queue");
        }
        if (!playsite.isFull() && queue.isEmpty()) {
            playsite.appendChild(child);
        } else {
            Ticket childticket = child.getTicket();
            int addAt = queue.size();
            if (childticket.canSkip()) {
                int nextVipIndex = getLastVipIndex() + 3 + 1;
                // add index must be not at end of queue to skip
                if(addAt > nextVipIndex) {
                    addAt = nextVipIndex;
                    childticket.incrementSkipCount();
                }
            }
            queue.add(addAt, child);
        }
    }

    /**
     * Returns the index of last VIP ticket. The operation iterates current queue in reverse to find last
     * such entity. If there is none, it returns -3.
     *
     * @return Last index of VIP ticket in queue or -3 when none is present.
     */
    protected int getLastVipIndex() {
        for (int i = queue.size() - 1; i >= 0; i--) {
            if (queue.get(i).getTicket().isVip()) {
                return i;
            }
        }
        return -4;
    }

    @Override
    public void advance() {
        if (!queue.isEmpty()) {
            Child child = queue.remove(0);
            playsite.appendChild(child);
        }
    }

    @Override
    public void remove(Child child) {
        queue.remove(child);
    }

    @Override
    public Collection<Child> getCurrentQueue() {
        return new LinkedList<>(queue);
    }
}
