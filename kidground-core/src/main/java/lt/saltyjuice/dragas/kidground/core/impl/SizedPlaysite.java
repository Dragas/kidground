package lt.saltyjuice.dragas.kidground.core.impl;

import lt.saltyjuice.dragas.kidground.api.Child;
import lt.saltyjuice.dragas.kidground.api.Playsite;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Playsite with limitation how many children can be present at single point
 */
public class SizedPlaysite implements Playsite {
    private final int size;
    private final Queue<Child> children = new LinkedList<>();
    public SizedPlaysite(int size) {
        if(size < 1) {
            throw new IllegalArgumentException("Size must be positive");
        }
        this.size = size;
    }
    @Override
    public Child appendChild(Child child) {
        // should it assert that same child is getting added?
        // migrating to Set<T> would solve this but then you lose
        // deque API.
        // can't sort this too as it must be FIFO
        // blah
        Child removedChild = null;
        if(children.size() >= size) {
            removedChild = children.poll();
        }
        if(child != null) {
            children.add(child);
        }
        return removedChild;
    }

    @Override
    public boolean isFull() {
        return size == children.size();
    }

    @Override
    public Child removeOldestChild() {
        return children.poll();
    }

    @Override
    public BigDecimal getCurrentUsage() {
        return BigDecimal.valueOf(children.size()).divide(BigDecimal.valueOf(size), 10, RoundingMode.HALF_UP);
    }

    @Override
    public void removeChild(Child child) {
        children.remove(child);
    }
}
