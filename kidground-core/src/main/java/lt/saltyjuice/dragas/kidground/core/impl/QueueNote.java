package lt.saltyjuice.dragas.kidground.core.impl;

import lt.saltyjuice.dragas.kidground.api.Child;
import lt.saltyjuice.dragas.kidground.api.Playsite;

import java.time.Instant;

public class QueueNote {
    private final Instant from;
    private final Playsite playsite;
    private final Child child;
    private Instant to;

    public QueueNote(Child child, Playsite playsite, Instant from) {
        this.child = child;
        this.playsite = playsite;
        this.from = from;
    }

    public Instant getFrom() {
        return from;
    }

    public Playsite getPlaysite() {
        return playsite;
    }

    public Child getChild() {
        return child;
    }

    public Instant getTo() {
        return to;
    }

    public void setTo(Instant to) {
        this.to = to;
    }
}
