package lt.saltyjuice.dragas.kidground.core.impl;

import lt.saltyjuice.dragas.kidground.api.Child;
import lt.saltyjuice.dragas.kidground.api.Playsite;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Provides historized tracking of playsite usage
 */
public class HistorizedPlaysite implements Playsite {
    private final Playsite playsite;

    private final Clock clock;

    private final Collection<QueueNote> history = new LinkedList<>();
    public HistorizedPlaysite(Clock clock, Playsite playsite) {
        this.playsite = playsite;
        this.clock = clock;
    }

    @Override
    public Child appendChild(Child child) {
        Instant now = clock.instant();
        QueueNote note = new QueueNote(child, playsite, now);
        history.add(note);
        Child removedChild = playsite.appendChild(child);
        if(removedChild != null) {
            note = history.stream().filter(it -> it.getChild() == removedChild && it.getTo() == null).findFirst().orElseThrow(() -> new IllegalStateException("FATAL: No previous note for removed child"));
            note.setTo(now);
        }
        return removedChild;
    }
    @Override
    public boolean isFull() {
        return playsite.isFull();
    }

    @Override
    public Child removeOldestChild() {
        return playsite.removeOldestChild();
    }

    @Override
    public BigDecimal getCurrentUsage() {
        return playsite.getCurrentUsage();
    }

    @Override
    public void removeChild(Child child) {
        playsite.removeChild(child);
    }

    /**
     * Returns view of current history for underlying playsite
     * @return copy of history collection
     */
    public Collection<QueueNote> getHistory() {
        return new LinkedList<>(history);
    }
}
