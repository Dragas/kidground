package lt.saltyjuice.dragas.kidground.core.impl;

import lt.saltyjuice.dragas.kidground.api.Child;
import lt.saltyjuice.dragas.kidground.api.Ticket;

public class MutableChild implements Child {
    private String name;
    private int age;
    private Ticket ticket;
    private boolean canWaitInQueue;

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    @Override
    public boolean canWaitInQueue() {
        return canWaitInQueue;
    }

    public void setCanWaitInQueue(boolean canWaitInQueue) {
        this.canWaitInQueue = canWaitInQueue;
    }

    @Override
    public String toString() {
        return String.format("MutableChild{VIP=%s}", getTicket().isVip());
    }
}
