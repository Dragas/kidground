package lt.saltyjuice.dragas.kidground.core.impl;

import lt.saltyjuice.dragas.kidground.api.Ticket;

/**
 * Represents ticket that child should present when trying to enter a queue.
 */
public class MutableTicket implements Ticket {
    private final String number;
    private int skipCount = 0;
    private final int maxSkipCount;

    public MutableTicket(String number, int maxSkipCount) {
        this.number = number;
        this.maxSkipCount = maxSkipCount;
    }

    @Override
    public String getNumber() {
        return number;
    }

    @Override
    public void incrementSkipCount() {
        skipCount++;
    }

    @Override
    public boolean canSkip() {
        return maxSkipCount > skipCount;
    }

    @Override
    public boolean isVip() {
        return maxSkipCount > 0;
    }
}
