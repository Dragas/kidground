package lt.saltyjuice.dragas.kidground.test;

import lt.saltyjuice.dragas.kidground.api.Child;
import lt.saltyjuice.dragas.kidground.api.Playsite;
import lt.saltyjuice.dragas.kidground.core.impl.MutableChild;
import lt.saltyjuice.dragas.kidground.core.playsite.BallPit;
import lt.saltyjuice.dragas.kidground.core.playsite.Carousel;
import lt.saltyjuice.dragas.kidground.core.playsite.Slide;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.stream.Stream;

public class SizedPlaysiteTest {
    private static final int PLAYSITE_DEFAULT_SIZE = 10;

    @ParameterizedTest
    @MethodSource("createPlaysiteStream")
    public void preconfiguredPlaysiteAcceptsConfiguredAmountOfPeople(Playsite playsite) {
        assertPlaysiteCyclesChildren(playsite, PLAYSITE_DEFAULT_SIZE);
    }

    @Test
    public void zeroSizedPlaysiteCannotBeCreated() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new BallPit(0);
        }, "Must not be able to create nonpositive sized playsite");
    }

    @Test
    public void negativeSizedPlaysiteCannotBeCreated() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new BallPit(-1);
        }, "Must not be able to create negative sized playsite");
    }

    @ParameterizedTest
    @MethodSource("createPlaysiteStream")
    public void emptyPlaysiteReturnsZeroUtilization(Playsite playsite) {
        BigDecimal utilization = playsite.getCurrentUsage();
        Assertions.assertEquals(BigDecimal.ZERO.divide(BigDecimal.ONE, 10, RoundingMode.HALF_UP), utilization, "Should be 0");
    }

    @ParameterizedTest
    @MethodSource("createPlaysiteStream")
    public void singleChildPlaysiteReturns10PercentUtilization(Playsite playsite) {
        Child child = new MutableChild();
        playsite.appendChild(child);
        BigDecimal utilization = playsite.getCurrentUsage();
        // test smell: reimplemented same thing twice
        // then again this avoids the nonsense that comes with IEEE754
        // so it makes sense that this is how you test division
        Assertions.assertEquals(BigDecimal.valueOf(1).divide(BigDecimal.valueOf(PLAYSITE_DEFAULT_SIZE), 10, RoundingMode.HALF_UP), utilization, "Should be 1/10");
    }
    @ParameterizedTest
    @MethodSource("createPlaysiteStream")
    public void removeChildReturnsAddedChild(Playsite playsite) {
        Child child = new MutableChild();
        playsite.appendChild(child);
        Child removedChild = playsite.removeOldestChild();
        Assertions.assertEquals(child, removedChild, "Must be same child as added");
    }

    public static void assertPlaysiteCyclesChildren(Playsite playsite, int maxCount) {
        Child firstChild = new MutableChild();
        Assertions.assertNull(playsite.appendChild(firstChild), "Result must be null");
        for (int i = 1; i < maxCount; i++) {
            Child child = new MutableChild();
            Assertions.assertNull(playsite.appendChild(child), "Result must be null");
        }
        Child lastChild = new MutableChild();
        Assertions.assertEquals(firstChild, playsite.appendChild(lastChild), "Result must same as first child");
    }

    public static Stream<Playsite> createPlaysiteStream() {
        return Stream.of(new BallPit(PLAYSITE_DEFAULT_SIZE), new Carousel(PLAYSITE_DEFAULT_SIZE), new Slide(PLAYSITE_DEFAULT_SIZE));
    }
}
