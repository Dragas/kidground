package lt.saltyjuice.dragas.kidground.test;

import java.time.*;

/**
 * Controllable clock implementation which advances forward in time
 * using preconfigured {@link java.time.Duration} with each call
 * to {@link #instant()}.
 *
 * This clock ignores {@link ZoneId} so any operations related to timezones
 * will fail. As a default it's bound to UTC.
 *
 * This implementation is useful mainly in tests as it makes the clock related
 * processes more predictable.
 */
public class PredictableClock extends Clock {
    private final Duration delta;
    private Instant now;

    public PredictableClock(Instant now, Duration delta) {
        this.now = now;
        this.delta = delta;
    }

    /**
     * Always returns UTC
     * @return {@link ZoneOffset#UTC}
     */
    @Override
    public ZoneId getZone() {
        return ZoneOffset.UTC;
    }

    /**
     * NOOP
     * @param zone
     * @return this
     */
    @Override
    public Clock withZone(ZoneId zone) {
        return this;
    }

    @Override
    public Instant instant() {
        now = now.plus(delta);
        return now;
    }
}
