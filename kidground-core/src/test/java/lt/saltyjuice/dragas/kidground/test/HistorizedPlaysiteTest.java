package lt.saltyjuice.dragas.kidground.test;

import lt.saltyjuice.dragas.kidground.api.Child;
import lt.saltyjuice.dragas.kidground.api.Playsite;
import lt.saltyjuice.dragas.kidground.core.impl.HistorizedPlaysite;
import lt.saltyjuice.dragas.kidground.core.impl.MutableChild;
import lt.saltyjuice.dragas.kidground.core.impl.QueueNote;
import lt.saltyjuice.dragas.kidground.core.playsite.BallPit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;

public class HistorizedPlaysiteTest {

    private Playsite playsite;
    private HistorizedPlaysite historizedPlaysite;
    private Clock clock;
    private final Instant now = Instant.now(Clock.systemUTC()); // taken from Clock.systemUTC() clock
    private final Duration singleMinute = Duration.ofMinutes(1);

    @BeforeEach
    public void setup() {
        this.playsite = new BallPit(1); // much simpler to test with as it does not require as many ops
        this.clock = new PredictableClock(now, singleMinute);
        this.historizedPlaysite = new HistorizedPlaysite(clock, playsite);
    }

    @Test
    public void addingChildCreatesNonFinishedEntry() {
        Child child = new MutableChild();
        historizedPlaysite.appendChild(child);
        Collection<QueueNote> notes = historizedPlaysite.getHistory();
        QueueNote note = notes.stream().findFirst().get(); // will throw anyways if not empty so no need to assertFalse(isEmpty)
        assertNoteStructure(child, note);
        Assertions.assertNull(note.getTo(), "To date must be null");
    }

    @Test
    public void historizedPlaysiteDelegatesCalls() {
        Child child = new MutableChild();
        playsite.appendChild(child);
        Assertions.assertEquals(playsite.isFull(), historizedPlaysite.isFull(), "Must return same result");
        Assertions.assertEquals(playsite.getCurrentUsage(), historizedPlaysite.getCurrentUsage(), "Must return same result");
        // assume underlying playsite remove oldest child is correct
        Assertions.assertEquals(child, historizedPlaysite.removeOldestChild(), "Must return same result");
        Assertions.assertEquals(playsite.getCurrentUsage(), historizedPlaysite.getCurrentUsage(), "Must return same result");
    }

    @Test
    public void addingTwoChildrenFinishesFirstEntry() {
        Child child = new MutableChild();
        Child child2 = new MutableChild();
        historizedPlaysite.appendChild(child);
        historizedPlaysite.appendChild(child2);
        Collection<QueueNote> history = historizedPlaysite.getHistory();
        Assertions.assertEquals(2, history.size(), "Two calls should result in two notes getting added");
        QueueNote note = history.stream().findFirst().get();
        assertNoteStructure(child, note);
        Assertions.assertEquals(now.plus(singleMinute).plus(singleMinute), note.getTo(), "To date must have delta added twice");
    }

    private void assertNoteStructure(Child child, QueueNote note) {
        Assertions.assertEquals(child, note.getChild(), "Must be same child object");
        Assertions.assertEquals(playsite, note.getPlaysite(), "Must be same playsite object");
        Assertions.assertEquals(now.plus(singleMinute), note.getFrom(), "From date must be a single minute in the future");
    }
}
