package lt.saltyjuice.dragas.kidground.test;

import lt.saltyjuice.dragas.kidground.api.Child;
import lt.saltyjuice.dragas.kidground.api.Playsite;
import lt.saltyjuice.dragas.kidground.core.impl.MutableChild;
import lt.saltyjuice.dragas.kidground.core.playsite.DoubleSwing;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static lt.saltyjuice.dragas.kidground.test.SizedPlaysiteTest.assertPlaysiteCyclesChildren;

/**
 * Tests dedicated to double swing since its different impl
 */
public class DoubleSwingTest {

    private Playsite swing;

    @BeforeEach
    public void setup() {
        swing = new DoubleSwing();
    }

    @Test
    public void doubleSwingAcceptsTwoPeople() {
        assertPlaysiteCyclesChildren(swing, 2);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1})
    public void doubleSwingReturnsZeroUsageOnZeroChildren(int childCount) {
        for (int i = 0; i < childCount; i++) {
            Child child = new MutableChild();
            swing.appendChild(child);
        }
        BigDecimal usage = swing.getCurrentUsage();
        Assertions.assertEquals(BigDecimal.ZERO.divide(BigDecimal.ONE, 10, RoundingMode.HALF_UP), usage, "Usage must be 0");
    }
    @Test
    public void doubleSwingReturnsFullUsageOnTwoChildren() {
        int childCount = 2;
        for (int i = 0; i < childCount; i++) {
            Child child = new MutableChild();
            swing.appendChild(child);
        }
        BigDecimal usage = swing.getCurrentUsage();
        Assertions.assertEquals(BigDecimal.ONE.divide(BigDecimal.ONE, 10, RoundingMode.HALF_UP), usage, "Usage must be 1");
    }
}
