package lt.saltyjuice.dragas.kidground.test;

import lt.saltyjuice.dragas.kidground.api.*;
import lt.saltyjuice.dragas.kidground.core.impl.DefaultQueue;
import lt.saltyjuice.dragas.kidground.core.impl.MutableChild;
import lt.saltyjuice.dragas.kidground.core.impl.MutableTicket;
import lt.saltyjuice.dragas.kidground.core.playsite.BallPit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;

public class QueueTest {

    private Playsite playsite;
    private PlaysiteQueue queue;

    @BeforeEach
    public void setup() {
        playsite = new BallPit(1);
        queue = new DefaultQueue(playsite);
    }

    @Test
    public void childIsAddedToQueueWhenPlaysiteIsFull() throws TantrumException {
        Child child = createChild(0);
        Child child1 = createChild(0);
        playsite.appendChild(child);
        queue.enqueue(child1);
        Collection<Child> children = queue.getCurrentQueue();
        Assertions.assertTrue(children.contains(child1), "Second child must be in queue");
    }

    @Test
    public void childIsAddedToPlaysiteWhenPlaysiteIsNotFull() throws TantrumException {
        Child child = createChild(0);
        queue.enqueue(child);
        Collection<Child> children = queue.getCurrentQueue();
        Assertions.assertTrue(children.isEmpty(), "Queue must be empty");
        Assertions.assertTrue(playsite.isFull(), "Current playsite must be full");
    }

    @Test
    public void childSkipsLineWithVIPTicket() throws TantrumException {
        Child vip = createChild(1);
        Child playingChild = createChild(0);
        Child normalChild = createChild(0);
        queue.enqueue(playingChild);
        queue.enqueue(normalChild);
        queue.enqueue(vip);
        Assertions.assertFalse(vip.getTicket().canSkip(), "Ticket must now be void");
        Collection<Child> children = queue.getCurrentQueue();
        Child firstChild = children.stream().findFirst().get();
        Assertions.assertEquals(vip, firstChild, "First child must be same VIP");
    }

    @Test
    public void childThrowsTantrumWhenPlaysiteIsNotEmpty() throws TantrumException {
        Child problemChild = createChild(0, false);
        Child normalChild = createChild(0);
        queue.enqueue(normalChild);
        Assertions.assertThrows(TantrumException.class, () -> queue.enqueue(problemChild), "Child must throw tantrum");
    }

    @Test
    public void childThrowsTantrumWhenQueueIsNotEmpty() throws TantrumException {
        Child problemChild = createChild(0, false);
        Child normalChild = createChild(0);
        Child normalChild2 = createChild(0);
        queue.enqueue(normalChild);
        queue.enqueue(normalChild2);
        Assertions.assertThrows(TantrumException.class, () -> queue.enqueue(problemChild), "Child must throw tantrum");
    }

    @Test
    public void skipIsNotConsumedWhenAddingAtEndOfLine() throws TantrumException {
        Child vip = createChild(1);
        Child normalChild = createChild(0);
        queue.enqueue(normalChild);
        queue.enqueue(vip);
        Assertions.assertTrue(vip.getTicket().canSkip(), "Must still be able to skip later");
    }

    @Test
    public void vipMustOnlyBePresentEvery4Positions() throws TantrumException {
        queue.enqueue(createChild(0));
        //basically the VNNNVNNNVNNN scenario
        for(int i = 0; i < 12; i++) {
            int skipCount = 0;
            if(i % 4 == 0) {
                skipCount = 1;
            }
            Child child = createChild(skipCount);
            queue.enqueue(child);
        }
        Collection<Child> children = queue.getCurrentQueue();
        int position = 0;
        for (Child child : children) {
            if(position % 4 == 0) {
                Assertions.assertTrue(child.getTicket().isVip(), "Child must have VIP ticket at index" + position);
            }
            else {
                Assertions.assertFalse(child.getTicket().isVip(), "Child not have VIP ticket at index " + position);
            }
            position++;
        }
    }

    private Child createChild(int ticketSkipCount) {
        return createChild(ticketSkipCount, true);
    }

    private Child createChild(int ticketSkipCount, boolean canWaitInqueue) {
        MutableChild child = new MutableChild();
        Ticket ticket = new MutableTicket("E", ticketSkipCount);
        child.setTicket(ticket);
        child.setCanWaitInQueue(canWaitInqueue);
        return child;
    }
}
