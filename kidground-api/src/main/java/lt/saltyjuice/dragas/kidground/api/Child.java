package lt.saltyjuice.dragas.kidground.api;

/**
 * Represents child object in the system. Implementations that work with this
 * should assert {@link #canWaitInQueue()} and throw {@link TantrumException}
 * when the child does not intend to wait in queue.
 *
 * All fields must be present, otherwise the object is considered to be malformed.
 */
public interface Child {
    String getName();

    int getAge();

    Ticket getTicket();

    boolean canWaitInQueue();
}
