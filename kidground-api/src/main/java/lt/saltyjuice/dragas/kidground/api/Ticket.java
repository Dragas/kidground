package lt.saltyjuice.dragas.kidground.api;

/**
 * Represents ticket object in the system. Implementations that work with this
 * should give priority to entities when {@link #canSkip()} returns true as
 * it is considered to be a "vip" ticket.
 *
 * All fields must be present, otherwise the object is considered to be malformed.
 */
public interface Ticket {
    String getNumber();

    void incrementSkipCount();

    boolean canSkip();

    boolean isVip();
}
