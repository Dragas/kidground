package lt.saltyjuice.dragas.kidground.api;

/**
 * General exception for children related issues that cannot be solved (easily)
 */
public class TantrumException extends Exception {
    public TantrumException() {
    }

    public TantrumException(String message) {
        super(message);
    }

    public TantrumException(String message, Throwable cause) {
        super(message, cause);
    }

    public TantrumException(Throwable cause) {
        super(cause);
    }

    public TantrumException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
