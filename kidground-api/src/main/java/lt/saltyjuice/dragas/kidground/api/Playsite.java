package lt.saltyjuice.dragas.kidground.api;

import java.math.BigDecimal;

public interface Playsite {
    /**
     * Adds a child to playsite and removes the oldest element from underlying collection,
     * if it's over predefined size.
     * @param child child to add to playsite
     * @return the oldest child when playsite is overlimit, otherwise null
     */
    Child appendChild(Child child);

    /**
     * Returns true when playsite is full.
     * @return true, when playsite is full.
     */
    boolean isFull();

    /**
     * Removes oldest child from the playsite
     * @return
     */
    Child removeOldestChild();

    void removeChild(Child child);

    /**
     * Returns current usage of the playsite. Usually it's current children count
     * divided by maximum size.
     * @return (current children count) / maximum
     */
    public BigDecimal getCurrentUsage();
}
