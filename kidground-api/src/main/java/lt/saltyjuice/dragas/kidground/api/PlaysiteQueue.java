package lt.saltyjuice.dragas.kidground.api;

import java.util.Collection;

/**
 * Represents queue to a playsite. It is expected that implementations
 * will assert whether {@link Child} will wait in line, whether the
 * {@link Child} has a VIPTicket, and it links to some {@link Playsite}
 * implementation under the hood.
 */
public interface PlaysiteQueue {
     /**
      * Adds child to underlying queue to destination {@link Playsite}
      * @param child Child to append to queue
      * @throws TantrumException when child does not permit waiting in queue
      */
     void enqueue(Child child) throws TantrumException;

     /**
      * Advances the underlying queue to destination playsite. If underlying queue is empty,
      * the operation should be noop.
      */
     void advance();

     /**
      * Removes the provided child from queue if it's present.
      * @param child Child to remove
      */
     void remove(Child child);

     /**
      * Returns view of current queue to playsite
      * @return view of current queue
      */
     Collection<Child> getCurrentQueue();
}
