# Kidground

Children playsite management library with tantrum support.

## Usage

### Dependency

```xml
<dependency>
    <groupId>lt.saltyjuice.dragas.kidground</groupId>
    <artifactId>kidground-core</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```
*Not really hosted anywhere*

### Bootstrapping

Design wise, integration is up to the user, as the library provides
primitives to create domain models. General usecase will be to manage
a queue and history of playsite usage. An example below sets up a queue to desired playsite

```java
Playsite playsite = new Ballpit(4); // one of 4 currently provided implementations
// must wrap with HistorizedPlaysite to track history
HistorizedPlaysite historized = new HistorizedPlaysite(Clock.systemUTC(), playsite);
PlaysiteQueue queue = new DefaultQueue(historized);
//the following in some ticking executor
queue.advance();
```

With the above, all movement from `DefaultQueue` will get tracked in `HistorizedPlaysite`
which is backed by delegate `Playsite` implementation (one of `Ballpit`, `Carousel`, `DoubleSwing`, `Slide`).

Since tracking is not limited to particular playsite, users are free to implement their
own. Refer to `SizedPlaysite` and `DoubleSwing` implementations.

It is recommended that from this point on, users would use the `PlaysiteQueue` object
to interact with the playsite as it includes lifecycle method `advance()` which moves
children from queue to playsite.

In order to add a child to some queue you must give it a ticket.
```java
MutableChild child = new MutableChild();
Ticket ticket = new MutableTicket("number", 0);
child.setTicket(ticket);
//later somewhere in the process
queue.enqueue(child);
```
It is also possible to circumvent the queue by adding the child into playsite directly
via `Playsite#appendChild(Child)`.

For more usage examples, check the tests in `kidground-core` module.

## Building

Run the package goal in maven
```bash
./mvnw package
```
Maven will take care of the rest